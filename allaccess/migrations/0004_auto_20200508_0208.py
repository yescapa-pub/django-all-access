# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0001_initial'),
        ('allaccess', '0003_auto_20150825_0929'),
    ]

    operations = [
        migrations.AlterField(
            model_name='provider',
            name='site',
            field=models.ForeignKey(default=None, blank=True, to='sites.Site', null=True, on_delete=models.CASCADE),
        ),
    ]

